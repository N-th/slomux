import { reducer, initialState } from '../reducers/index'

export const createStore = (reducer, initialState) => {
    let currentState = initialState
    const listeners = []
    const getState = () => currentState
    const dispatch = action => {
        currentState = reducer(currentState, action)
        listeners.forEach(listener => listener())
    }
    const subscribe = listener => listeners.push(listener)
    return { getState, dispatch, subscribe }
}
// т.к. раньше иcпользовала createstore из redux то решила сделать по аналогии
export const store = createStore(reducer, initialState)
