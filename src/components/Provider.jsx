import React from 'react'
// тут возникли сложности понимания, как изначально устроен компонент Provider, он должен получать данные из store
class Provider extends React.Component {
    componentWillMount() {
        const { store } = this.props
        return store
    }

    render() {
        return this.props.children
    }
}

export default Provider
