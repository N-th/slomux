import React from 'react'

class ToDoComponent extends React.Component {
    state = {
        todoText: ''
    }

    render() {
//  данные сгрупированы для лучшей читабельности
        const { title, todos } = this.props
        const { todoText } = this.state
        return (
// label не был связан с input
//при генерации списка, для элементов нужен ключ, для уникальности (концепция React)
            <div>
                <label htmlFor="inputTodo">
                    {title || 'Без названия'}
                </label>
                <div>
                    <input
                        id="inputTodo"
                        value={todoText}
                        placeholder="Название задачи"
                        onChange={this.updateText}
                        />
                    <button onClick={this.addTodo}>Добавить</button>
                    <ul>
                        {todos.map((todo, idx) => <li key={idx}>{todo}</li>)}
                    </ul>
                </div>
            </div>
    )
    }
// ошибка в методах updateText, addTodo: теряли контекст при вызове и использовали прямой доступ к свойствам состояния
        updateText = (e) => {
            const { value } = e.target
            this.setState({ todoText: value })
        }
//  данные сгрупированы для лучшей читабельности и единобразности
        addTodo = () => {
            const { addTodo } = this.props
            const { todoText } = this.state

            addTodo(todoText)
            this.setState({ todoText: '' })
        }
    }

export default ToDoComponent
