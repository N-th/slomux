import {ADD_TODO} from '../actions/addTodo'
export const initialState = [];

export const reducer = (state = initialState, action) => {
    switch(action.type) {
        case ADD_TODO:
// reducer должен возвращать новый state, а не менять предыдущий
            return [...state, action.payload]
        default:
            return state
    }
};
