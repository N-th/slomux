import ToDoComponent from '../components/SlomuxUi'
import { connect } from '../hocs/connect'
import { addTodo }  from '../actions/addTodo'

export const ToDo = connect(state => ({
    todos: state,
}), dispatch => ({
    addTodo: text => dispatch(addTodo(text)),
}))(ToDoComponent)
