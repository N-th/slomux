import React from 'react'
import ReactDOM from 'react-dom'
import Provider from './components/Provider'
import { createStore } from './store/configureStore'
import { ToDo } from './containers/Slomux'

ReactDOM.render(
    <Provider store={createStore}>
        <ToDo title="Список задач" />
    </Provider>,
    document.getElementById('app')
);
